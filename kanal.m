function[gains h_t]=kanal(N)

% This is for Rayleigh frequency-selective fading, which assumes complex
% Gaussian matrix elements with in-phase and quadrature components independent.
% Assume iid matrix channel elements, and further, independent channel taps

A=[1 1/exp(1) 1/exp(2) 1/exp(3) 1/exp(4)]; %vector containing the power-delay profile (real values)
%N=4096; %number of subchannels

%Time Domain: define the channel

h_int1 = randn(length(A), 1) + 1i*randn(length(A), 1);
h_int2=[];
for i = 1:length(A)
    h_int2 = [h_int2;sqrt(A(i)/2)*h_int1((i-1)+1:i,:)];
end


h_t=[h_int2;zeros((N-length(A)),1)];

%Channel will be transformed from Time Domain to Frequency Domain
%using Fourier Transform
H_f = zeros(1,N);
H_f = fft(h_t(1:1:(N-1)+1,1));

%Since S is a matrix, S(i) is the gain of the i-th subchannel.

S=abs(H_f)';
gains=S/max(S);
%gains=S;
%gains=gains(2:length(gains));


