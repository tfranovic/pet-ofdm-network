classdef relay
    
    properties
        position=[0,0];
        subs=[];
        fsubs=[];
        sreqs=[];
        nsubc=0;
        bsubc=0;
        req=0;
        rcv=0;
        f=0;
    end
    
    methods
        function obj=relay(x,y,nsubc,bsubc)
            obj.position=[x,y];
            obj.nsubc=nsubc;
            obj.bsubc=bsubc;
        end
        
        function obj=add_subscriber(obj,sub,f, r)
            obj.subs=[obj.subs,sub];
            obj.fsubs=[obj.fsubs;f];
            obj.sreqs=[obj.sreqs, r];
            obj.req=obj.req+r;
        end
        
        function obj=add_subcarrier(obj, subc)
            obj.rcv=obj.rcv+floor(obj.f(subc));
        end
        
        function obj=clear(obj)
            obj.subs=[];
            obj.fsubs=[];
            obj.req=0;
            obj.rcv=0;
            obj.f=0;
        end
        
        function [subs, reqs, recvs]=assign_smart(obj)
            assigned=0;
            subs=obj.subs;
            act_subs=1:length(obj.subs);
            reqs=obj.sreqs;
            recvs=zeros(1,length(subs));
            done_subs=[];
            for i=1:obj.nsubc
                if isempty(act_subs) && ~isempty(done_subs)
                    bestj=1;
                    for j=1:length(done_subs)
                        if obj.fsubs(done_subs(j))>obj.fsubs(done_subs(bestj))
                            bestj=j;
                        end
                    end
                    if assigned+floor(obj.fsubs(done_subs(bestj),i))>obj.rcv
                        continue;
                    end
                    recvs(done_subs(bestj))=recvs(done_subs(bestj))+floor(obj.fsubs(done_subs(bestj),i));
                    assigned=assigned+floor(obj.fsubs(done_subs(bestj),i));
                elseif ~isempty(act_subs)
                    bestj=1;
                    for j=1:length(act_subs)
                        if obj.fsubs(act_subs(j))>obj.fsubs(act_subs(bestj))
                            bestj=j;
                        end
                    end
                    if assigned+floor(obj.fsubs(act_subs(bestj),i))>obj.rcv
                        continue;
                    end
                    recvs(act_subs(bestj))=recvs(act_subs(bestj))+floor(obj.fsubs(act_subs(bestj),i));
                    assigned=assigned+floor(obj.fsubs(act_subs(bestj),i));
                    if recvs(act_subs(bestj))>=reqs(act_subs(bestj))
                        done_subs=[done_subs,act_subs(bestj)];
                        act_subs(bestj)=[];
                    end
                end
            end
        end
        
        function [subs, reqs, recvs]=assign_random(obj)
            assigned=0;
            subs=obj.subs;
            act_subs=1:length(obj.subs);
            reqs=obj.sreqs;
            recvs=zeros(1,length(subs));
            for i=1:obj.nsubc
                if ~isempty(act_subs)
                    bestj=randi(length(act_subs),1);
                    if assigned+floor(obj.fsubs(act_subs(bestj),i))>obj.rcv
                        continue;
                    end
                    recvs(act_subs(bestj))=recvs(act_subs(bestj))+floor(obj.fsubs(act_subs(bestj),i));
                    assigned=assigned+floor(obj.fsubs(act_subs(bestj),i));
                end
            end
        end
    end
    
end