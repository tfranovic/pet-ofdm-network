function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 30-Jan-2013 16:20:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(handles.ax_bs,'xtick',[])
set(handles.ax_bs,'ytick',[])
set(handles.ax_bs,'xticklabel',[])
set(handles.ax_bs,'yticklabel',[])
% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_BS.
function btn_BS_Callback(hObject, eventdata, handles)
% hObject    handle to btn_BS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bs
bs=base_station(str2num(get(handles.r1,'String')),str2num(get(handles.r2,'String')),str2num(get(handles.n_rel,'String')),str2num(get(handles.n_subs,'String')),str2num(get(handles.snr,'String')),str2num(get(handles.n_subc,'String')));
cla(handles.ax_bs,'reset')
bs.draw_station(handles.ax_bs)
set(handles.ax_bs,'xtick',[])
set(handles.ax_bs,'ytick',[])
set(handles.ax_bs,'xticklabel',[])
set(handles.ax_bs,'yticklabel',[])
set(handles.btn_sim,'Enable','on');

function r1_Callback(hObject, eventdata, handles)
% hObject    handle to r1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of r1 as text
%        str2double(get(hObject,'String')) returns contents of r1 as a double


% --- Executes during object creation, after setting all properties.
function r1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to r1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function r2_Callback(hObject, eventdata, handles)
% hObject    handle to r2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of r2 as text
%        str2double(get(hObject,'String')) returns contents of r2 as a double


% --- Executes during object creation, after setting all properties.
function r2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to r2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function n_subs_Callback(hObject, eventdata, handles)
% hObject    handle to n_subs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of n_subs as text
%        str2double(get(hObject,'String')) returns contents of n_subs as a double


% --- Executes during object creation, after setting all properties.
function n_subs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to n_subs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function n_rel_Callback(hObject, eventdata, handles)
% hObject    handle to n_rel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of n_rel as text
%        str2double(get(hObject,'String')) returns contents of n_rel as a double


% --- Executes during object creation, after setting all properties.
function n_rel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to n_rel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function snr_Callback(hObject, eventdata, handles)
% hObject    handle to snr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of snr as text
%        str2double(get(hObject,'String')) returns contents of snr as a double


% --- Executes during object creation, after setting all properties.
function snr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to snr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function n_subc_Callback(hObject, eventdata, handles)
% hObject    handle to n_subc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of n_subc as text
%        str2double(get(hObject,'String')) returns contents of n_subc as a double


% --- Executes during object creation, after setting all properties.
function n_subc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to n_subc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b_subc_Callback(hObject, eventdata, handles)
% hObject    handle to b_subc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b_subc as text
%        str2double(get(hObject,'String')) returns contents of b_subc as a double


% --- Executes during object creation, after setting all properties.
function b_subc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b_subc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_sim.
function btn_sim_Callback(hObject, eventdata, handles)
% hObject    handle to btn_sim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bs
cla(handles.ax_sim,'reset');
set(handles.tx_smart,'String','');
set(handles.tx_random,'String','');
set(handles.c_smart,'String','');
set(handles.c_random,'String','');
set(handles.lbl_sim,'Visible','on');
drawnow;
[t1,t2,t3]=bs.simulate(str2num(get(handles.n_steps,'String')),str2num(get(handles.n_sim,'String')));
axes(handles.ax_sim);
if str2num(get(handles.n_steps,'String'))>1
    plot(mean(t1),'-gs');
    hold on;
    plot(mean(t2),'-bs');
    plot(mean(t3),'-rs');
    legend('Zahtjevi','Smart','Random');
    set(handles.tx_smart,'String',num2str(round(100*sum(mean(t2)))/100));
    set(handles.tx_random,'String',num2str(round(100*sum(mean(t3)))/100));
    c1=corrcoef(mean(t1),mean(t2));
    c2=corrcoef(mean(t1),mean(t3));
else
    plot(t1,'-gs');
    hold on;
    plot(t2,'-bs');
    plot(t3,'-rs');
    legend('Zahtjevi','Smart','Random');
    set(handles.tx_smart,'String',num2str(round(100*sum(t2))/100));
    set(handles.tx_random,'String',num2str(round(100*sum(t3))/100));
    c1=corrcoef(t1,t2);
    c2=corrcoef(t1,t3);
end
if str2num(get(handles.n_subs,'String'))>1
    set(handles.c_smart,'String',num2str(round(100*c1(2))/100));
    set(handles.c_random,'String',num2str(round(100*c2(2))/100));
end
set(handles.lbl_sim,'Visible','off');
drawnow;

function n_steps_Callback(hObject, eventdata, handles)
% hObject    handle to n_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of n_steps as text
%        str2double(get(hObject,'String')) returns contents of n_steps as a double


% --- Executes during object creation, after setting all properties.
function n_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to n_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function n_sim_Callback(hObject, eventdata, handles)
% hObject    handle to n_sim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of n_sim as text
%        str2double(get(hObject,'String')) returns contents of n_sim as a double


% --- Executes during object creation, after setting all properties.
function n_sim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to n_sim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tx_smart_Callback(hObject, eventdata, handles)
% hObject    handle to tx_smart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tx_smart as text
%        str2double(get(hObject,'String')) returns contents of tx_smart as a double


% --- Executes during object creation, after setting all properties.
function tx_smart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tx_smart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tx_random_Callback(hObject, eventdata, handles)
% hObject    handle to tx_random (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tx_random as text
%        str2double(get(hObject,'String')) returns contents of tx_random as a double


% --- Executes during object creation, after setting all properties.
function tx_random_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tx_random (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function c_smart_Callback(hObject, eventdata, handles)
% hObject    handle to c_smart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of c_smart as text
%        str2double(get(hObject,'String')) returns contents of c_smart as a double


% --- Executes during object creation, after setting all properties.
function c_smart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_smart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function c_random_Callback(hObject, eventdata, handles)
% hObject    handle to c_random (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of c_random as text
%        str2double(get(hObject,'String')) returns contents of c_random as a double


% --- Executes during object creation, after setting all properties.
function c_random_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_random (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
