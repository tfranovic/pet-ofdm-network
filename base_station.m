classdef base_station
    
    properties
        r1=300;
        r2=150;
        nrel=5;
        nsub=10;
        relays=[];
        subscribers=[];
        snr=100;
        nsubc=0;
        bsubc=0;
        position=[0 0];
    end
    
    methods
        function obj=base_station(r1,r2,n_relays,n_subscribers,snr,n_subcarriers)
            obj.position=[0 0];
            obj.r1=r1;
            obj.r2=r2;
            obj.nrel=n_relays;
            obj.nsub=n_subscribers;
            obj.snr=10^(snr/10);
            obj.nsubc=n_subcarriers;
            obj.bsubc=floor(log2(1+snr));
            obj=obj.create_relays();
            obj=obj.create_subscribers();
        end
        
        function obj=create_relays(obj)
            obj.relays=[];
            for i=1:obj.nrel
                a=(2*pi/obj.nrel)*(i-1);
                x=obj.r2*cos(a);
                y=obj.r2*sin(a);
                obj.relays=[obj.relays; relay(x,y,obj.nsubc,obj.bsubc)];
            end
        end
        function obj=create_subscribers(obj)
            obj.subscribers=[];
            for i=1:obj.nsub
                obj.subscribers=[obj.subscribers; subscriber(obj.r1, obj.r2)]; 
            end
        end
        
        function draw_station(obj,ax)
            if nargin<2
                ax=figure();
            end
            axes(ax);
            scatter(0,0,'k','filled');
            hold on;
            ps=[];
            for i=1:length(obj.subscribers)
                ps=[ps;obj.subscribers(i).position];
            end
            scatter(ps(:,1),ps(:,2),'b','filled');
            pr=[];
            for i=1:length(obj.relays)
                pr=[pr;obj.relays(i).position];
            end
            scatter(pr(:,1),pr(:,2),'r','filled');
            
            t = 0:0.001:2*pi;
            x1 = cos(t)*obj.r1;
            y1 = sin(t)*obj.r1;
            plot(x1,y1,'b')
            x2 = cos(t)*obj.r2;
            y2 = sin(t)*obj.r2;
            plot(x2,y2,'r')
        end
        
        function [t_reqs_s, t_recvs_s, t_recvs_r] = simulate(obj,nsteps,nsims)
            t_reqs_s=zeros(nsteps,obj.nsub);
            t_recvs_s=zeros(nsteps,obj.nsub);
            t_reqs_r=zeros(nsteps,obj.nsub);
            t_recvs_r=zeros(nsteps,obj.nsub);
            for i=1:nsteps
                act_subs=[];
                for j=1:length(obj.subscribers)
                    if rand<obj.subscribers(j).msg_prob
                        act_subs=[act_subs, j];
                    end
                end
                [e_reqs_s,e_recvs_s]=obj.process_smart(act_subs,nsims);
                [e_reqs_r,e_recvs_r]=obj.process_random(act_subs,nsims);
                t_reqs_s(i,:)=e_reqs_s;
                t_recvs_s(i,:)=e_recvs_s;
                t_reqs_r(i,:)=e_reqs_r;
                t_recvs_r(i,:)=e_recvs_r;
            end
        end
        
        function [e_reqs,e_recvs] = process_smart(obj,act_subs,nsims)
            for i=1:obj.nrel
                obj.relays(i)=obj.relays(i).clear(); % clear relays
            end
            scale=0;
            for i=1:length(act_subs)
                scale=scale+obj.subscribers(act_subs(i)).min_bits;
            end
            scale=scale/0.9; %skaliranje zahtjeva
            for i=1:length(act_subs)
                s=act_subs(i);
                [r,f]=obj.pick_relay(nsims); 
                obj.relays(r)=obj.relays(r).add_subscriber(s,f,obj.subscribers(s).min_bits*obj.nsubc*obj.bsubc/scale); 
            end
            act_relays=[];
            for i=1:obj.nrel
                if obj.relays(i).req>0
                    act_relays=[act_relays,i];
                    obj.relays(i).f=base_station.get_gains(obj.snr,obj.nsubc,nsims);
                end
            end
            done_relays=[];
            for i=1:obj.nsubc
                if isempty(act_relays) && ~isempty(done_relays)
                    bestj=1;
                    for j=1:length(done_relays)
                        if obj.relays(done_relays(j)).f(i)>obj.relays(done_relays(bestj)).f(i)
                            bestj=j;
                        end
                    end
                    obj.relays(done_relays(bestj))=obj.relays(done_relays(bestj)).add_subcarrier(i);
                elseif ~isempty(act_relays)
                    bestj=1;
                    for j=1:length(act_relays)
                        if obj.relays(act_relays(j)).f(i)>obj.relays(act_relays(bestj)).f(i)
                            bestj=j;
                        end
                    end
                    obj.relays(act_relays(bestj))=obj.relays(act_relays(bestj)).add_subcarrier(i);
                    if(obj.relays(act_relays(bestj)).rcv>=obj.relays(act_relays(bestj)).req)
                        done_relays=[done_relays,act_relays(bestj)];
                        act_relays(bestj)=[];
                    end
                end
            end
            s_subs=[];
            s_reqs=[];
            s_recvs=[];
            for i=1:obj.nrel
                if obj.relays(i).req>0
                    [s,re,rc]=obj.relays(i).assign_smart();
                    s_subs=[s_subs,s];
                    s_reqs=[s_reqs,re];
                    s_recvs=[s_recvs,rc];
                end
            end
            e_reqs=zeros(1,obj.nsub);
            e_recvs=zeros(1,obj.nsub);
            e_reqs(s_subs)=s_reqs;
            e_recvs(s_subs)=s_recvs;
        end
        
        function [e_reqs,e_recvs]=process_random(obj,act_subs,nsims)
            for i=1:obj.nrel
                obj.relays(i)=obj.relays(i).clear(); % clear relays
            end
            scale=0;
            for i=1:length(act_subs)
                scale=scale+obj.subscribers(act_subs(i)).min_bits;
            end
            scale=scale/0.9; %skaliranje zahtjeva
            for i=1:length(act_subs)
                s=act_subs(i);
                r=randi(obj.nrel,1);
                f=floor(base_station.get_gains(obj.snr,obj.nsubc,1));
                obj.relays(r)=obj.relays(r).add_subscriber(s,f,obj.subscribers(s).min_bits*obj.nsubc*obj.bsubc/scale); 
            end
            act_relays=[];
            for i=1:obj.nrel
                if obj.relays(i).req>0
                    act_relays=[act_relays,i];
                    obj.relays(i).f=floor(base_station.get_gains(obj.snr,obj.nsubc,1));
                end
            end
            for i=1:obj.nsubc
                if ~isempty(act_relays)
                    bestj=randi(length(act_relays),1);
                    obj.relays(act_relays(bestj))=obj.relays(act_relays(bestj)).add_subcarrier(i);
                end
            end
            s_subs=[];
            s_reqs=[];
            s_recvs=[];
            for i=1:obj.nrel
                if obj.relays(i).req>0
                    [s,re,rc]=obj.relays(i).assign_random();
                    s_subs=[s_subs,s];
                    s_reqs=[s_reqs,re];
                    s_recvs=[s_recvs,rc];
                end
            end
            e_reqs=zeros(1,obj.nsub);
            e_recvs=zeros(1,obj.nsub);
            e_reqs(s_subs)=s_reqs;
            e_recvs(s_subs)=s_recvs;
        end
        
        function [relay, func]=pick_relay(obj, nsims)
            maxv=0;
            best=0;
            bestf=0;
            for i=1:obj.nrel
                f=base_station.get_gains(obj.snr,obj.nsubc,nsims);
                v=sum(f);
                if v>maxv
                    maxv=v;
                    best=i;
                    bestf=f;
                end
            end
            relay=best;
            func=floor(bestf);
        end
    end
    methods(Static)
        function dist=e_dist(p1,p2)
            dist=sqrt((p1(1)-p2(1))^2+(p1(2)-p2(2))^2);
        end
        
        function gains=get_gains(snr,nsubc,nsims)
            gains=zeros(nsims,nsubc);
            for i=1:nsims
                gains(i,:)=log2(1+snr.*abs(kanal(nsubc)).^2);
            end
            if(nsims>1)
                gains=mean(gains);
            end
        end
    end
end