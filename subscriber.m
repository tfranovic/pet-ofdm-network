classdef subscriber

    properties
        position=[0,0];
        msg_prob=0.0;
        min_bits=0;
    end
    
    methods
        function obj=subscriber(r1, r2)
            a=2*pi*rand;
            r=sqrt(rand);
            x=((r1-r2)*r+r2)*cos(a);
            y=((r1-r2)*r+r2)*sin(a);
            obj.position=[x,y];
            obj.msg_prob=rand;
            obj.min_bits=rand;
        end
    end
    
end

